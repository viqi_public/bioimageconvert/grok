#pragma once

/*
 ==========================================================
   Common constants shared among several modules
 ==========================================================
*/
#define OPJ_COMMON_CBLK_DATA_EXTRA        2    /**< Margin for a fake FFFF marker */

#include "opj_includes.h"
